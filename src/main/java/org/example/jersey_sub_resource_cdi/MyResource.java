package org.example.jersey_sub_resource_cdi;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("")
public class MyResource {

	@Inject @Named("name")
	private String name;
	
	@Context
	private ResourceContext context;
	
	@GET @Path("test")
	public Response test() {
		return Response.ok("Name in root resource: " + name).build();
	}
	
	@Path("test2")
	public Object test2() {
		return context.getResource(MySubResource.class);
		// return MySubResource.class; // works as well
	}
	
}

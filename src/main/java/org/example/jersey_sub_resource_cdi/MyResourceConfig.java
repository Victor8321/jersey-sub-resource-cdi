package org.example.jersey_sub_resource_cdi;

import org.glassfish.jersey.server.ResourceConfig;

class MyResourceConfig extends ResourceConfig {

	MyResourceConfig() {
		packages(true, "org.example.jersey_sub_resource_cdi");
		// register(MySubResource.class); // either use this or annotate MySubResource with @Provider or @Path(..)
	}
	
}

package org.example.jersey_sub_resource_cdi;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider // use this or register() this class in your ResourceConfig
public class MySubResource {

	@Inject @Named("name")
	private String name;
	
	@Context
	private HttpServletRequest request;
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response test() {
		return Response.ok(
			"Name in sub resource: " + name + "<br>" +
			"Request (JAX-RS resource): " + request
		).build();
	}
	
}

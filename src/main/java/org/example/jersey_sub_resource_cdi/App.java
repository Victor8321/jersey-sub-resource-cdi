package org.example.jersey_sub_resource_cdi;

class App {
	
	public static void main(String... args) {
		new AppModule()
			.buildApp()
			.run();
	}
}

package org.example.jersey_sub_resource_cdi;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

@ApplicationScoped
class TestProducer {

	@Produces @Named("name")
	String getName() {
		return "John";
	}
	
}

package org.example.jersey_sub_resource_cdi;

import org.glassfish.jersey.servlet.ServletContainer;
import org.jboss.weld.environment.servlet.Listener;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;

class AppModule {

	Runnable buildApp() {

		DeploymentInfo deployment = Servlets.deployment();

		deployment
			.setClassLoader(App.class.getClassLoader())
			.setContextPath("/")
			.setDeploymentName("jerseySubResourceCdi")
			.addListeners(Servlets.listener(Listener.class))
			.addServlets(Servlets.servlet("jerseyServlet", ServletContainer.class)
				.setLoadOnStartup(1)
				.addInitParam("javax.ws.rs.Application", MyResourceConfig.class.getName())
				.addMapping("/*")
			);

		DeploymentManager manager = Servlets.defaultContainer()
			.addDeployment(deployment);
		manager.deploy();

		return () -> {
			
			PathHandler path;
			try {
				path = Handlers
					.path(Handlers.redirect("/"))
					.addPrefixPath("/", manager.start());
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			
			Undertow server = Undertow.builder()
				.addHttpListener(5000, "localhost")
				.setHandler(path)
				.build();
			server.start();
		};
	}
	
}
